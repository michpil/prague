collection = [i for i in range(100, 0, -1)]


def project_phases(number_list):
    mapped_number_list = []
    for i in number_list:
        divisible_by_5 = i % 5 == 0
        divisible_by_3 = i % 3 == 0
        divisible_by_5_and_3 = divisible_by_5 and divisible_by_3

        mapping = {divisible_by_5: 'Agile', divisible_by_3: 'Software', divisible_by_5_and_3: 'Testing'}
        result = mapping.get(True, i)

        mapped_number_list.append(result)
        print(result)

    return mapped_number_list


if __name__ == "__main__":
    project_phases(collection)
